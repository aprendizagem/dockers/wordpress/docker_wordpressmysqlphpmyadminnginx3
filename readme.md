# Instruções

## Instalação
<p> Faça o clone do código </p>

```
git clone link_do_repositorio
```

Após baixar o código execute o comando

```
docker-compose up -d --build
```
* O comando -d ( para funcionar aplicação em background)
* o comando --build para gerar a imagem

Após isso execute o comando

```
chmod -R 777 nome_da_pasta_onde_esta_sua_aplicacao
```

Espere um pouco para atualizar as pasta <strong>dbdata</strong> e <strong>wordpress</strong>  e depois inicie sua aplicação. Pode acessar pelo browse

```
PHPMYADMIN -> localhost:8093
WORDPRESS ->  localhost:8091
```

Pode alterar as portas no <strong>docker-compose.yaml </strong>

## Na parte instalar plugin no wordpress AUTH para wordpress API

Plugin que vou usar [LINK](https://br.wordpress.org/plugins/jwt-authentication-for-wp-rest-api/)
<p> As vezes pede acesso ftp para resolver esse problema </p>

<p>Com uma pesquisa rápida no Google você encontra a solução de mudar o dono da pasta do site (no momento deve estar “root” ou “seu-usuario”):</p>

```
$ sudo chown -R www-data pasta-do-site
```
```
$ sudo chmod -R g+w pasta-do-site
```

<p>A primeira linha altera recursivamente o dono da pasta para “www-data” (geralmente esse é o usuário atribuído ao seu localhost, se não for, troque o nome) e a segunda linha torna isso verdade para as novas subpastas, caso crie alguma.</p>

<p>O problema dessa solução é que muito provavelmente você não poderá mais criar ou alterar manualmente pastas e arquivos (através da interface gráfica, por exemplo).</p>



## Na hora de ignorar algumas pastas

Tente ignorar a pasta dbdata para upar para o repositório. Sempre que possível gere um arquivo sql do banco de dados e salva na pasta SQL.

O motivo é porque vai ficar muito grande seu repositório.


# Como ignorar a pasta

[Onde aprendi](https://gist.github.com/kelvinst/7d508da482d13bb301c9)

## Como fazer um `.gitignore` local?

Bom, este é um recurso, como muitos outros, bem escondido do git. Então resolvi fazer um post para explicar a situação em que pode-se usar e como fazer essa magia negra. :ghost:

## O problema

Você provavelmente já adicionou algum dia um arquivo no projeto que não deveria ser commitado certo? E como você fez para ignorar esse arquivo mesmo? Provavelmente adicionou no arquivo `.gitignore`.

OK então, aí você commitou esse arquivo `.gitignore` e pronto, mais ninguém poderá criar um arquivo com o mesmo nome e commitar. Mas espera aí! Não era isso que você queria! Você só queria ignorar esse arquivo na sua máquina, se alguém, algum dia por obséquio achar esse um nome bom para seu arquivo, que assim seja.

Então como fazer isso? Não commitar o arquivo `.gitignore` e colocar o `.gitignore` dentro do `.gitignore` para não commitar ele por quando tiver alteração. Bom, essa opção se você pensar um pouco vai notar porque não funciona: se você disser para o git ignorar o `.gitignore`, como é que você vai commitar o `.gitignore` com o `.gitignore` ignorado (nossa, quanta ignorância :grin:).

OK, como posso fazer então?

## A solução!

Então, aqui vai uma maneira para você fazer isso. Em todo repositório git existe um arquivo `.git/info/exclude`. Ele funciona exatamente como um arquivo `.gitignore` só que ele não é commitado! Então é só colocar uma linha com o nome do seu arquivo nele e :tada:!

Pronto, assim você consegue ignorar arquivos no seu repositório e só nele, sem passar a configuração para seus coleguinhas! 

Espero que tenham gostado da dica. Até a próxima!